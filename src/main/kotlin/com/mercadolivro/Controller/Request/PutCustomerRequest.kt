package com.mercadolivro.Controller.Request

data class PutCustomerRequest (
    var name: String,

    var email : String
)
