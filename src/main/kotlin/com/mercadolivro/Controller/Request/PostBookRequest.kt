package com.mercadolivro.Controller.Request

import com.fasterxml.jackson.annotation.JsonAlias
import java.math.BigDecimal

data class PostBookRequest(
    var name: String,

    var price: BigDecimal,

    @JsonAlias("customer_id") // referente ao customerId
    var customerId: Int
) {

}
