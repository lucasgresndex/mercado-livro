package com.mercadolivro.Controller

import com.mercadolivro.Controller.Request.PostBookRequest
import com.mercadolivro.Controller.Request.PutBookRequest
import com.mercadolivro.Controller.response.BookResponse
import com.mercadolivro.Service.BookService
import com.mercadolivro.Service.CustomerService
import com.mercadolivro.extension.toBookModel
import com.mercadolivro.extension.toResponse
import org.springframework.data.domain.Page
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.awt.print.Pageable


@RestController
@RequestMapping("book") //referencia o caminho
class BookController(
    val bookService: BookService,
    val customerService: CustomerService
) {


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody request: PostBookRequest) {
        val customer = customerService.findById(request.customerId) //recupera o customer
        bookService.create(request.toBookModel(customer)) //seta dentro do book
    }

    @GetMapping
    fun findAll(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponse> {
        return bookService.findAll(pageable).map { it.toResponse() }
    }

    @GetMapping("/active")
    fun findActive(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponse> {
        return bookService.findActive(pageable)
            .map { it.toResponse() } //se quiser pode     apagar as chaves e remover o metodo return, so colocar um igual na frente do List<BookModel>
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Int): BookResponse {
        return bookService.findById(id).toResponse()
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable id: Int) {
        bookService.delete(id)

    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(@PathVariable id: Int, @RequestBody book: PutBookRequest) {
        val bookSave = bookService.findById(id)
        bookService.update(book.toBookModel(bookSave)) //pega o livro salvo e atualiza
    }

}