package com.mercadolivro.Controller.response

data class FieldErrorResponse(

    var message: String,

    var field: String
) {

}
