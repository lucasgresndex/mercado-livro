package com.mercadolivro.Controller

import com.mercadolivro.Controller.Request.PostCustomerRequest
import com.mercadolivro.Controller.Request.PutCustomerRequest
import com.mercadolivro.Controller.response.CustomerResponse
import com.mercadolivro.Service.CustomerService
import com.mercadolivro.extension.toCustomerModel
import com.mercadolivro.extension.toResponse
import com.mercadolivro.model.CustomerModel
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("customer") //referencia o caminho



class CustomerController(
    //injeta as dependencias
    val customerService: CustomerService

) {

    @GetMapping
        fun getAll(@RequestBody name: String?): List<CustomerResponse> {
            return customerService.getAll(name).map { it.toResponse() } //passa por todos os registros e faz a transformação dentro das chaves.
        }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
        fun create(@RequestBody customer: PostCustomerRequest){
            customerService.create(customer.toCustomerModel())
        }

    @GetMapping("/{id}")
        fun getCustomer(@PathVariable id: Int): CustomerResponse {
            return customerService.findById(id).toResponse() //retorna a modelagem do BD. Não é bom pq tem coisas que não queremos retornar
        }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
        fun update(@PathVariable id: Int, @RequestBody customer: PutCustomerRequest){ //Recebe uma id e o customer
            val customerSaved = customerService.findById(id)
            customerService.update(customer.toCustomerModel(customerSaved))
        }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
        fun delete(@PathVariable id: Int) {
            customerService.delete(id)
    }

}



