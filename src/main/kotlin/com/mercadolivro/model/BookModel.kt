package com.mercadolivro.model

import com.mercadolivro.enums.BookStatus
import com.mercadolivro.enums.Errors
import com.mercadolivro.exception.BadRequestException
import jakarta.persistence.*
import java.math.BigDecimal

//como se comuncia com a tabela
@Entity(name = "book")
data class BookModel(
    @Id //primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
    var id: Int? = null,

    @Column //se a variavel fosse outro nome, tinha que abrir entre parenteses e igualar, tipo Column(nome = "name")
    var name: String,

    @Column
    var price: BigDecimal,



    @ManyToOne //muitos livros para um usuário
    @JoinColumn(name = "customer_id") //Coluna referente a outra tabela
    var customer: CustomerModel? = null



) {
    @Column
    @Enumerated(EnumType.STRING)
    var status: BookStatus? = null
        set(value){ //sobreescreve o metodo, recebendo o novo valor do atributo
            if(field == BookStatus.CANCELED || field == BookStatus.REMOVED){
                throw BadRequestException(Errors.ML0102.message.format(field), Errors.ML0102.code)
            }
            field = value

        }
    constructor(id : Int? = null,
                name: String,
                price: BigDecimal,
                customer: CustomerModel? = null,
                status: BookStatus?):this(id, name, price, customer){
        this.status = status
                }

}

