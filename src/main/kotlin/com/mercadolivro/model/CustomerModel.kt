package com.mercadolivro.model

import com.mercadolivro.enums.CustomerStatus
import jakarta.persistence.*

//como se comuncia com a tabela
@Entity(name = "customer")
data class CustomerModel(

    @Id //primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
    var id: Int? = null,

    @Column //se a variavel fosse outro nome, tinha que abrir entre parenteses e igualar, tipo Column(nome = "name")
    var name: String,

    @Column
    var email: String,

    @Column
    @Enumerated(EnumType.STRING)
    var status: CustomerStatus

)


