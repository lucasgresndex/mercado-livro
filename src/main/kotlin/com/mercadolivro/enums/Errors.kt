package com.mercadolivro.enums

enum class Errors(val code: String, val message: String) {

    ML0001("ML-0001", "Customer [%S] not exists!"),

    ML0101("ML-0101", "Book [%S] not exists!"),
    ML0102("ML-0102", "Cannot update book with status [%s]")

}