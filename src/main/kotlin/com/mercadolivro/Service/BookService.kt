package com.mercadolivro.Service

import com.mercadolivro.enums.BookStatus
import com.mercadolivro.enums.Errors
import com.mercadolivro.exception.NotFoundException
import com.mercadolivro.model.BookModel
import com.mercadolivro.model.CustomerModel
import com.mercadolivro.repository.BookRepository
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import java.awt.print.Pageable


@Service
class BookService(
    val bookRepository: BookRepository
) {

    fun create(book: BookModel) {
        bookRepository.save(book)
    }

    fun findAll(pageable: Pageable): Page<BookModel> {
       return bookRepository.findAll(pageable)
    }

    fun  findActive(pageable: Pageable): Page<BookModel> {
        return bookRepository.findByStatus(BookStatus.ACTIVE, pageable)
    }

    fun findById(id: Int): BookModel {
        return bookRepository.findById(id).orElseThrow { NotFoundException(Errors.ML0101.message.format(id), Errors.ML0101.code) }
    }

    fun delete(id: Int) {
        val book = findById(id)//nao vou apagar os livros da base

        book.status = BookStatus.CANCELED

        update(book)
    }

    fun update(book: BookModel) {
        bookRepository.save(book)
    }

    fun deleteByCostumer(customer: CustomerModel) {
        bookRepository.findByCustomer(customer)
        val books = bookRepository.findByCustomer(customer)
        for (book in books) { // para cada registro na lista books
            book.status = BookStatus.REMOVED

        }
        bookRepository.saveAll(books)
    }


}
