package com.mercadolivro.Service

import com.mercadolivro.enums.CustomerStatus
import com.mercadolivro.enums.Errors
import com.mercadolivro.exception.NotFoundException
import com.mercadolivro.model.CustomerModel
import com.mercadolivro.repository.CustomerRepository
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable


@Service //ele é um servico
class CustomerService(
    val customerRepository: CustomerRepository,
    val bookService: BookService
) {
    val customers = mutableListOf<CustomerModel>()

    // @GetMapping
    //vamos filtrar
    fun getAll(name: String?): List<CustomerModel> { //o nome pode vir preenchido ou n
        name?.let { //se minha variavel não for nula
            //return customers.filter { it.name.contains(name, true) }
            return customerRepository.findByNameContaining(it)
        }
        return customerRepository.findAll().toList()
    }

    // @PostMapping
    // @ResponseStatus(HttpStatus.CREATED)
    fun create(customer: CustomerModel) {
        /* val id = if(customers.isEmpty()){
             "1"
         } else {
             customers.last().id!!.toInt() + 1 //transforma a string em um numero para que não fique 1, 11, 111. Ai ele somará com isso
         }*/ //não importa mais, ja que tem o banco de dados

        customerRepository.save(customer)

    }

    //@GetMapping("/{id}") //pega qualquer id que pertencer ao usuario e exibe as info
    fun findById(id: Int): CustomerModel {
        //return  customers.filter { it.id == id }.first() //it se refere ao Lucas. Esse return quer pegar o id do usuário e verificar se é igual ao que eu procuro
        return customerRepository.findById(id).orElseThrow {
            NotFoundException(
                Errors.ML0001.message,
                Errors.ML0001.code
            )
        } // pode ou não ter o registro. Estouraremos uma exception
    }

    //@PutMapping("/{id}")
    //@ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(customer: CustomerModel) { //Recebe uma id e o customer
        /*customers.filter { it.id == customer.id }.first().let {
            it.name = customer.name
            it.email = customer.email*/
        if (!customerRepository.existsById(customer.id!!)) {
            throw Exception()
        }
        customerRepository.save(customer)//também utilizado para salvar

    }

    //@DeleteMapping("/{id}")
    // @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable id: Int) {

        val customer = findById(id)
        bookService.deleteByCostumer(customer)

        customer.status = CustomerStatus.INACTIVE

        customerRepository.save(customer)
    }
}